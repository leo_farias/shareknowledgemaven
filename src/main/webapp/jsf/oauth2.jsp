<%@page import="com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest"%>
<%@page import="com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse"%>
<%@page import="com.google.api.client.auth.oauth2.Credential"%>
<%@page import="com.google.api.client.auth.oauth2.TokenResponse"%>
<%@page import="com.google.api.services.samples.youtube.cmdline.GoogleAuthHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Share Knowledge Oauth</title>
</head>
<body>
	<div class="oauthDemo">
		<%
			/*
			 * The GoogleAuthHelper handles all the heavy lifting, and contains all "secrets"
			 * required for constructing a google login url.
			 */
			

                        if (request.getParameter("code") != null) {

				//session.removeAttribute("state");

				out.println("<pre>");
				/*
				 * Executes after google redirects to the callback url.
				 * Please note that the state request parameter is for convenience to differentiate
				 * between authentication methods (ex. facebook oauth, google oauth, twitter, in-house).
				 * 
				 * GoogleAuthHelper()#getUserInfoJson(String) method returns a String containing
				 * the json representation of the authenticated user's information. 
				 * At this point you should parse and persist the info.
				 */
                                
                                final GoogleAuthHelper helper = new GoogleAuthHelper();
                                
				out.println(helper.getUserInfoJson(request.getParameter("code")));

				out.println("</pre>");
                                
                                
                                out.println("<script>window.location.href ='" + helper.buildLoginUrl()
						+ "';</script>");                              
			}
		%>
	</div>
</body>
</html>
