package com.google.api.services.samples.youtube.cmdline;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.samples.youtube.cmdline.live.CreateBroadcast;
import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A helper class for Google's OAuth2 authentication API.
 * @version 20130224
 * @author Matyas Danter
 */
public class GoogleAuthHelper {

	/**
	 * Please provide a value for the CLIENT_ID constant before proceeding, set this up at https://code.google.com/apis/console/
	 */
	private static final String CLIENT_ID = "186788328250-dkl4ej15dihgb6pfe8p1sr00g0q5un3p.apps.googleusercontent.com";
	/**
	 * Please provide a value for the CLIENT_SECRET constant before proceeding, set this up at https://code.google.com/apis/console/
	 */
	private static final String CLIENT_SECRET = "1B-toHsBa-XI2ODmyxOZqbih";
        
        /**
        * This is the directory that will be used under the user's home directory where OAuth tokens will be stored.
        */
        private static final String CREDENTIALS_DIRECTORY = "oauth-credentials";

	/**
	 * Callback URI that google will redirect to after successful authentication
	 */
	private static final String CALLBACK_URI = "http://localhost:41515/ShareKnowledgeMaven/faces/jsf/listarEventos.xhtml";
        
        private static final String CALLBACK_URI2 = "http://localhost:41515/ShareKnowledgeMaven/faces/jsf/oauth2.jsp";
	
	// start google authentication constants
	private static final Collection<String> SCOPE = Arrays.asList("https://www.googleapis.com/auth/youtube.force-ssl".split(";"));
	private static final String USER_INFO_URL = "https://www.googleapis.com/auth/youtube.force-ssl";
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	// end google authentication constants
	
	private String stateToken;
        
	private GoogleAuthorizationCodeFlow flow;
        
        public Credential credential;
	
	/**
	 * Constructor initializes the Google Authorization Code Flow with CLIENT ID, SECRET, and SCOPE 
	 */
	public GoogleAuthHelper() throws IOException {
            
            FileDataStoreFactory fileDataStoreFactory = new FileDataStoreFactory(new File(System.getProperty("user.home") + "/" + CREDENTIALS_DIRECTORY));
            DataStore<StoredCredential> datastore = fileDataStoreFactory.getDataStore("createbroadcast");

            // This creates the credentials datastore at ~/.oauth-credentials/${credentialDatastore}
            flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
				JSON_FACTORY, CLIENT_ID, CLIENT_SECRET, SCOPE).setAccessType("offline").setApprovalPrompt("force").
                    setCredentialDataStore(datastore).build();
            generateStateToken();
	}

	/**
	* Builds a login URL based on client ID, secret, callback URI, and scope
        * @return */
	public String buildLoginUrl() {
		
		//final GoogleAuthorizationCodeRequestUrl url = getFlow().newAuthorizationUrl();
		
		return CALLBACK_URI;
	}
        
        /**
	* Builds a login URL based on client ID, secret, callback URI, and scope
        * @return */
	public String buildLoginUrl2() {
		
		final GoogleAuthorizationCodeRequestUrl url = getFlow().newAuthorizationUrl();
		
		return url.setRedirectUri(CALLBACK_URI2).build();
	}
	
	/**
	 * Generates a secure state token 
	 */
	private void generateStateToken(){
		
		SecureRandom sr1 = new SecureRandom();
		
		stateToken = "google;"+sr1.nextInt();
		
	}
	
	/**
	 * Accessor for state token
	 */
	public String getStateToken(){
		return stateToken;
	}
	
	/**
	 * Expects an Authentication Code, and makes an authenticated request for the user's profile information
	 * @return JSON formatted user profile information
	 * @param authCode authentication code provided by google
	 */
	public String getUserInfoJson(final String authCode) throws IOException {

		final GoogleTokenResponse response = getFlow().newTokenRequest(authCode).setRedirectUri(CALLBACK_URI2).execute();
		this.credential = getFlow().createAndStoreCredential(response, "user");
                
                CreateBroadcast.credential = getFlow().createAndStoreCredential(response, "user");
                
		final HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
		// Make an authenticated request
		final GenericUrl url = new GenericUrl(USER_INFO_URL);
		final HttpRequest request = requestFactory.buildGetRequest(url);
		request.getHeaders().setContentType("application/json");
		final String jsonIdentity = request.execute().parseAsString();

		return jsonIdentity;

	}

    /**
     * @return the flow
     */
    public GoogleAuthorizationCodeFlow getFlow() {
        return flow;
    }

    /**
     * @param flow the flow to set
     */
    public void setFlow(GoogleAuthorizationCodeFlow flow) {
        this.flow = flow;
    }

}
