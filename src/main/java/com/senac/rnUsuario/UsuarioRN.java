/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.rnUsuario;

import com.senac.bd.UsuarioBD;
import com.senac.bean.Usuario;
import com.senac.util.CrudGenerico;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Mateus
 */
@WebService
public class UsuarioRN {

    private UsuarioBD usuarioBD;
    
    public UsuarioRN() {
        usuarioBD = new UsuarioBD();
    }
    
    public void salvar(Usuario usuarioBean) {
        
        if(usuarioBean.getNome() == null || "".equals(usuarioBean.getNome())){
            throw new RuntimeException("Campo Nome deve ser Obrigatório!");
        }
        if (usuarioBean.getSobrenome()== null || "".equals(usuarioBean.getSobrenome())) {
            throw new RuntimeException("Campo Sobrenome deve ser Obrigatório!");
        }
        if (usuarioBean.getEmail()== null || "".equals(usuarioBean.getEmail())) {
            throw new RuntimeException("Campo Email deve ser Obrigatório!");
        }
        if (usuarioBean.getSenha()== null || "".equals(usuarioBean.getSenha())) {
            throw new RuntimeException("Campo Senha deve ser Obrigatório!");
        }
        usuarioBD.salvar(usuarioBean);
    }

    public void excluir(Usuario usuario) {
        usuarioBD.excluir(usuario);
    }

    public List<Usuario> listar(Usuario usuario) {        
        return usuarioBD.listar(usuario);
    }
    
    public List<Usuario> listarTudo(){
        return usuarioBD.listarTudo();
    }
       
    public Usuario buscar(Usuario bean) {
        return usuarioBD.consultar(bean);
    }
    
    public List<Usuario> find(String email, String senha) {
        return usuarioBD.findUsuario(email, senha);
    }

    public List<Usuario> pesquisa(String nome){        
        return usuarioBD.pesquisaUsuario(nome);
    }
}
