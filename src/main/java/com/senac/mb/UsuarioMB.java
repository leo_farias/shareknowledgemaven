/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.mb;

import com.senac.bean.Usuario;
import com.senac.rnUsuario.UsuarioRN;
import com.senac.util.Mensagem;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


/**
 *
 * @author Mateus
 */
@ManagedBean
@SessionScoped
public class UsuarioMB {

    private Usuario usuario;
    private UsuarioRN usuarioRN;
    private List<Usuario> usuarios;
    private String pesquisa;
    
    public UsuarioMB() {
           limparTudo();
    }
    
    public void limparTudo(){
        usuario = new Usuario();
        usuarioRN = new UsuarioRN();
        usuarios = usuarioRN.listar(usuario);  
        pesquisa = "";
    }

    public void editar(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String pesquisar(){
        try{
            usuarios = usuarioRN.pesquisa(pesquisa);
            Mensagem.add("Operação executada com sucesso!");
            return "listarUsuarios";
        } catch(Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;   
    }

    public List<Usuario> lista() {
        try {
            return usuarios;
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public String salvar() {
        try{
            usuarioRN.salvar(usuario);
            Mensagem.add("Operação executada com sucesso!");
            usuario = new Usuario();
            usuarios = usuarioRN.listar(usuario);
            return "listarUsuarios";
        }catch(Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public void limpar() {
        usuario = new Usuario();
    }
    
    public String excluir(Usuario u) {
        try {
            usuarioRN.excluir(u);
            Mensagem.add("Operação executada com sucesso!");
            usuario = new Usuario();
            usuarios = usuarioRN.listar(usuario);
            return "listarUsuarios";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;    
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }


    
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the pesquisa
     */
    public String getPesquisa() {
        return pesquisa;
    }

    /**
     * @param pesquisa the pesquisa to set
     */
    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }
    
    
    
}
