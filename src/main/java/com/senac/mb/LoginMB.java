package com.senac.mb;

import com.senac.bean.Usuario;
import com.senac.rnUsuario.UsuarioRN;
import com.senac.util.Mensagem;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class LoginMB {

    private String email;
    private String senha;
       
    public LoginMB() {
        
    }

    public String autentica() {
       try{         
           UsuarioRN usuarioRN = new UsuarioRN();
           List<Usuario> usuarios = usuarioRN.find(email, senha);
        if(!usuarios.isEmpty()){
            return "oauth.jsp";
        }
       }catch(Exception e){
            
        }
           Mensagem.error("Dados inválidos!"); 
           return "index";
       }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }
}
    
    

