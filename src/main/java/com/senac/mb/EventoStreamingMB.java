/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.mb;

import com.google.api.services.samples.youtube.cmdline.live.CreateBroadcast;
import com.senac.bean.Evento;
import com.senac.rnEvento.EventoRN;
import com.senac.util.Mensagem;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author MacLeo
 */
@ManagedBean
public class EventoStreamingMB {
    
    private Date minDate;
    private Date minDateEnd;
    private Evento evento;
    private EventoRN eventoRN;
    private String pesquisa;
    private List<Evento> eventos;
    
    public EventoStreamingMB() {
        limparTudo();
    }
    
    public void limparTudo(){
        this.minDate = new Date();
        evento = new Evento();
        eventoRN = new EventoRN();
        eventos = eventoRN.listar(evento);
        pesquisa = "";
    }
    
    public List<Evento> lista() {
        try {
            return eventos;
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;
    }
    
    public String salvar(){
        
        try {
            evento.setEnderecoBroadcast(CreateBroadcast.createStreaming(evento.getTituloEvento(), evento.getDescricaoEvento(), 
                    evento.getTituloStream(), evento.getDescricaoStream(),
                    evento.getDataInicio(), evento.getDataTermino()));
            eventoRN.salvar(evento);
            Mensagem.add("Operação executada com sucesso!");
            evento = new Evento();
            eventos = eventoRN.listar(evento); 
            return "listarEventos";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
       return null; 
    }
    
    public String pesquisar() {
        try {
            eventos = eventoRN.pesquisa(pesquisa);
            Mensagem.add("Operação executada com sucesso!");
            return "listarEventos";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public Date setMinEndDate(Date date){
        minDateEnd = minDate;
        minDateEnd.setMinutes(date.getMinutes()+10);
        return minDateEnd;
    }

    /**
     * @return the minDate
     */
    public Date getMinDate() {
        return minDate;
    }

    /**
     * @param minDate the minDate to set
     */
    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    /**
     * @return the minDateEnd
     */
    public Date getMinDateEnd() {
        return minDateEnd;
    }

    /**
     * @param minDateEnd the minDateEnd to set
     */
    public void setMinDateEnd(Date minDateEnd) {
        this.minDateEnd = minDateEnd;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public EventoRN getEventoRN() {
        return eventoRN;
    }

    public void setEventoRN(EventoRN eventoRN) {
        this.eventoRN = eventoRN;
    }

    /**
     * @return the pesquisa
     */
    public String getPesquisa() {
        return pesquisa;
    }

    /**
     * @param pesquisa the pesquisa to set
     */
    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    /**
     * @return the eventos
     */
    public List<Evento> getEventos() {
        return eventos;
    }

    /**
     * @param eventos the eventos to set
     */
    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }
    
    
}
