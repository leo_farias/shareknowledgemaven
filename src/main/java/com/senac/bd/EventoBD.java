/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.bd;

import com.senac.bean.Evento;
import com.senac.bean.Usuario;
import com.senac.util.CrudGenerico;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mateus
 */
public class EventoBD {
    private EntityManager em;
    
    public EventoBD() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ShareknowledgePU");
        em = emf.createEntityManager();
    }

    public void salvar(Evento evento) {
        em.getTransaction().begin();
        em.merge(evento);
        em.getTransaction().commit();
    }
    
    public void excluir(Evento evento) {
        em.getTransaction().begin();
        em.remove(em.find(Evento.class, evento.getIdEvento()));
        em.getTransaction().commit();
    }
    
    public List<Evento> listarTudo(){
        Query query = em.createNamedQuery("Evento.findAll");
        return query.getResultList();
    }

    public Usuario consultar(Evento evento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Evento> listar(Evento evento) {
        StringBuilder sb = new StringBuilder("SELECT e FROM Evento e");

        if (evento.getIdEvento()!= null && evento.getIdEvento() != 0) {
            sb.append(" and e.id_evento=:i");
        }
        if(evento.getTituloEvento() != null){
            sb.append(" and v.tituloevento like :v");
        }
        if(evento.getTituloStream() != null){
            sb.append(" and t.titulostream like :t");
        }
        if(evento.getDescricaoEvento() != null){
            sb.append(" and d.descricaoevento like :d");
        }
        if(evento.getDescricaoStream() != null){
            sb.append(" and s.descricaostream like :s");
        }

        Query qry = em.createQuery(sb.toString());
        if (evento.getIdEvento() != null && evento.getIdEvento() != 0) {
            qry.setParameter("i", evento.getIdEvento());
        }
        if (evento.getTituloEvento() != null) {
            qry.setParameter("v", "%" + evento.getTituloEvento() + "%");
        }
        if (evento.getTituloStream() != null) {
            qry.setParameter("t", "%" + evento.getTituloStream() + "%");
        }
        if (evento.getDescricaoEvento() != null) {
            qry.setParameter("d", "%" + evento.getDescricaoEvento() + "%");
        }
        if (evento.getDescricaoStream() != null) {
            qry.setParameter("s", "%" + evento.getDescricaoStream() + "%");
        }
        return qry.getResultList();
    }
    
    /**
     *
     * @param tituloEvento
     * @return
     */
    public List<Evento> pesquisaEvento(String tituloEvento) {
        Query query = em.createQuery("Select c FROM Evento c where c.tituloEvento = :tituloEvento", Evento.class);
        query.setParameter("tituloEvento", tituloEvento);
        return query.getResultList();
    }
    
    
}
