/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.rnEvento;

import com.senac.bd.EventoBD;

import com.senac.bean.Evento;
import com.senac.bean.Usuario;
import com.senac.util.CrudGenerico;

import java.util.List;

import javax.jws.WebService;

/**
 *
 * @author Mateus
 */
@WebService
public class EventoRN {

    private EventoBD eventoBD;

    public EventoRN() {
        eventoBD = new EventoBD();
    }

    public void salvar(Evento evento) {

        if (evento.getTituloEvento() == null || "".equals(evento.getTituloEvento())) {
            throw new RuntimeException("Campo Título deve ser Obrigatório!");
        }
        if (evento.getDescricaoEvento()== null || "".equals(evento.getDescricaoEvento())) {
            throw new RuntimeException("Campo Descrição deve ser Obrigatório!");
        }
        if (evento.getTituloStream() == null || "".equals(evento.getTituloStream())) {
            throw new RuntimeException("Campo Titulo deve ser Obrigatório!");
        }
        if (evento.getDescricaoStream() == null || "".equals(evento.getDescricaoStream())) {
            throw new RuntimeException("Campo Descrição deve ser Obrigatório!");
        }
        if (evento.getDataInicio() == null || "".equals(evento.getDataInicio())) {
            throw new RuntimeException("Campo Data de Inicio deve ser Obrigatório!");
        }
        if (evento.getDataTermino() == null || "".equals(evento.getDataTermino())) {
            throw new RuntimeException("Campo Data de Término deve ser Obrigatório!");
        }
        eventoBD.salvar(evento);
    }

    public void excluir(Evento evento) {
        eventoBD.excluir(evento);
    }

    public List<Evento> listar(Evento evento) {
        return eventoBD.listar(evento);
    }
    
    public List<Evento> listarTudo() {
        return eventoBD.listarTudo();
    }

    public Usuario buscar(Evento evento) {
        return null;
    }

    public List<Evento> pesquisa(String tituloEvento) {
        return eventoBD.pesquisaEvento(tituloEvento);
    }
    
}
